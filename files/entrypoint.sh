#!/bin/bash

echo "Substituindo variáveis para o site ${VIRTUAL_HOST}"
envsubst '${VIRTUAL_HOST}${API_URL}' < /etc/nginx/conf.d/default.conf.template > /etc/nginx/conf.d/${VIRTUAL_HOST}.conf

echo "Starting nginx..."
exec nginx -g "daemon off;"
