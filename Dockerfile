FROM nginx:alpine

COPY files/default.conf.template /etc/nginx/conf.d/default.conf.template
COPY files/entrypoint.sh entrypoint.sh

COPY files/ssl/options-ssl-nginx.conf /etc/ssl/options-ssl-nginx.conf
COPY files/ssl/nginx-selfsigned.crt /etc/ssl/certs/nginx-selfsigned.crt
COPY files/ssl/nginx-selfsigned.key /etc/ssl/private/nginx-selfsigned.key

RUN dos2unix entrypoint.sh
RUN dos2unix /etc/nginx/conf.d/default.conf.template
RUN dos2unix /etc/ssl/options-ssl-nginx.conf

RUN chmod 755 entrypoint.sh

RUN apk upgrade
RUN apk update
RUN apk add bash
RUN apk add bash-completion

#RUN apk add openssl
#RUN openssl dhparam -out /etc/ssl/certs/dhparam.pem 4096

ENTRYPOINT ["/entrypoint.sh"]
